package breakthrough.client;

import breakthrough.common.BTNames;
import breakthrough.domain.*;
import breakthrough.doubles.LocalMethodCallClientRequestHandler;
import breakthrough.marshalling.json.StandardJSONInvoker;
import frs.broker.*;
import frs.broker.marshall.json.StandardJSONRequestor;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by soren_kirkeby on 21/11/2017.
 */
public class TestMarshalling {
    private Breakthrough bt;
    private LocalMethodCallClientRequestHandler clientRequestHandler;

    @Before
    public void setUp() throws Exception {

        BreakthroughSurrogate breakthroughSurrogate = new BreakthroughSurrogate();

        Invoker invoker = new StandardJSONInvoker(breakthroughSurrogate);

        clientRequestHandler =
                new LocalMethodCallClientRequestHandler(invoker);
        Requestor requestor =
                new StandardJSONRequestor(clientRequestHandler);

        bt = new BreakthroughBrokerProxy(requestor);
    }

    @Test
    public void shouldVerifyMarshallingFormatForGetWinner() {
        bt.getWinner();

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(), is(BTNames.GET_WINNER));
        assertThat(req.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(), is("\"NONE\""));
    }

    @Test
    public void shouldVerifyMarshallingFormatForGetPieceAt() {
        bt.getPieceAt(new Position(1,1));

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(), is(BTNames.GET_PIECE_AT));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getPayload(), is("\"BLACK\""));

    }

    @Test
    public void shouldVerifyMarshallingFormatForMove() {
        Move m = new Move(new Position(6,0), new Position(5,0));
        bt.move(m);

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(), is(BTNames.MOVE));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getPayload(), is("true"));

    }

    @Test
    public void shouldVerifyMarshallingFormatForGetPlayerInTurn() {
        bt.getPlayerInTurn();

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(), is(BTNames.GET_PLAYER_IN_TURN));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getPayload(), is("\"WHITE\""));
    }
}
