package breakthrough.client;

import frs.broker.Requestor;

import java.lang.reflect.Type;

/**
 * Created by soren_kirkeby on 21/11/2017.
 */
public class SpyRequestor implements Requestor {
    String lastOperationName;
    Object[] lastArgument;
    String lastObjectId;
    Type lastType;

    @Override
    public <T> T sendRequestAndAwaitReply(String objectId, String operationName, Type typeOfReturnValue, Object... argument) {
        lastObjectId = objectId;
        lastOperationName = operationName;
        lastArgument = argument;
        lastType = typeOfReturnValue;
        if (typeOfReturnValue.equals(Boolean.class)) {
            // To avoid nullPointers when calling move
            return (T) Boolean.FALSE;
        }
        return null;
    }
}
