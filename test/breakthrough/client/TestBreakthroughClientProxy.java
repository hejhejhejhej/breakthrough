package breakthrough.client;

import breakthrough.common.BTNames;
import breakthrough.domain.Breakthrough;
import breakthrough.domain.Color;
import breakthrough.domain.Move;
import breakthrough.domain.Position;
import org.junit.Before;
import org.junit.Test;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by soren_kirkeby on 21/11/2017.
 */
public class TestBreakthroughClientProxy {
    SpyRequestor spy;
    Breakthrough bt;
    @Before
    public void setUp() throws Exception {
        // Test spy for client proxy
        spy = new SpyRequestor();
        bt = new BreakthroughBrokerProxy(spy);
    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForGetWinner() {
        bt.getWinner();

        assertThat(spy.lastOperationName, is(BTNames.GET_WINNER));
        assertThat(spy.lastType, sameInstance(Color.class));
    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForGetPieceAt() {
        Position p = new Position(1,1);
        bt.getPieceAt(p);

        assertThat(spy.lastOperationName, is(BTNames.GET_PIECE_AT));
        assertThat(spy.lastArgument[0], is(p));

    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForMove() {
        Move m = new Move(new Position(6,0), new Position(5,0));
        bt.move(m);

        assertThat(spy.lastType, sameInstance(Boolean.class));
        assertThat(spy.lastOperationName, is(BTNames.MOVE));
        assertThat(spy.lastArgument[0], is(m));
    }

    @Test
    public void shoudValidateClientProxyCallingRequestorForGetPlayerInTurn() {
        bt.getPlayerInTurn();

        assertThat(spy.lastType, sameInstance(Color.class));
        assertThat(spy.lastOperationName, is(BTNames.GET_PLAYER_IN_TURN));

    }
}
