package breakthrough.client;

import breakthrough.common.BTNames;
import breakthrough.domain.Breakthrough;
import breakthrough.domain.Color;
import breakthrough.domain.Move;
import breakthrough.domain.Position;
import frs.broker.Requestor;

/**
 * Created by soren_kirkeby on 21/11/2017.
 */
public class BreakthroughBrokerProxy implements Breakthrough, frs.broker.ClientProxy {

    private final Requestor requestor;

    public BreakthroughBrokerProxy(Requestor requestor) {
        this.requestor = requestor;
    }

    @Override
    public Color getPieceAt(Position p) {
        return requestor.sendRequestAndAwaitReply("",
                BTNames.GET_PIECE_AT,
                Color.class,
                p);
    }

    @Override
    public Color getPlayerInTurn() {
        return requestor.sendRequestAndAwaitReply("",
                BTNames.GET_PLAYER_IN_TURN,
                Color.class);
    }

    @Override
    public Color getWinner() {
        return requestor.sendRequestAndAwaitReply("",
                BTNames.GET_WINNER,
                Color.class);

    }

    @Override
    public boolean move(Move move) {
        return requestor.sendRequestAndAwaitReply("",
                        BTNames.MOVE,
                        Boolean.class,
                        move);
    }
}
