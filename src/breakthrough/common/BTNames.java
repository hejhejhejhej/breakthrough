package breakthrough.common;

/**
 * Created by soren_kirkeby on 21/11/2017.
 */
public class BTNames {
    public static final String GET_PIECE_AT = "get-piece-at";
    public static final String GET_WINNER = "get-winner";
    public static final String MOVE = "move";
    public static final String GET_PLAYER_IN_TURN = "get-player-in-turn";
}
