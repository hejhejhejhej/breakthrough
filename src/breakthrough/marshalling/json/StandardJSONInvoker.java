package breakthrough.marshalling.json;

import breakthrough.common.BTNames;
import breakthrough.domain.Breakthrough;
import breakthrough.domain.Color;
import breakthrough.domain.Move;
import breakthrough.domain.Position;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import frs.broker.Invoker;
import frs.broker.ReplyObject;


import javax.servlet.http.HttpServletResponse;

public class StandardJSONInvoker implements Invoker {

    private final Breakthrough breakthrough;
    private final Gson gson;

    public StandardJSONInvoker(Breakthrough breakthroughServant) {
        this.breakthrough = breakthroughServant;
        gson = new Gson();
    }

    @Override
    public ReplyObject handleRequest(String objectId,
                                     String operationName,
                                     String payloadJSONArray) {
        ReplyObject reply = null;

        // Demarshall parameters into a JsonArray
        JsonParser parser = new JsonParser();
        JsonArray array =
                parser.parse(payloadJSONArray).getAsJsonArray();

        try {
            // Dispatching on all known operations
            // Each dispatch follows the same algorithm
            // a) retrieve parameters from json array (if any)
            // b) invoke servant method
            // c) populate a reply object with return values

            switch (operationName) {
                case BTNames.GET_WINNER:

                    Color winner = breakthrough.getWinner();
                    reply = new ReplyObject(HttpServletResponse.SC_OK,
                            gson.toJson(winner));
                    break;
                case BTNames.GET_PIECE_AT:

                    Position p = gson.fromJson(array.get(0), Position.class);
                    Color piece = breakthrough.getPieceAt(p);
                    reply = new ReplyObject(HttpServletResponse.SC_OK,
                            gson.toJson(piece));
                    break;
                case BTNames.MOVE:
                    Move m = gson.fromJson(array.get(0), Move.class);
                    Boolean isLegalMove = breakthrough.move(m);
                    reply = new ReplyObject(HttpServletResponse.SC_OK,
                            gson.toJson(isLegalMove));
                    break;
                case BTNames.GET_PLAYER_IN_TURN:
                    Color playerInTurn = breakthrough.getPlayerInTurn();
                    reply = new ReplyObject(HttpServletResponse.SC_OK,
                            gson.toJson(playerInTurn));
                    break;
                default:
                    // Unknown operation
                    reply = new ReplyObject(HttpServletResponse.
                            SC_NOT_IMPLEMENTED,
                            "Server received unknown operation name: '"
                                    + operationName + "'.");
                    break;
            }


        } catch( Exception e ) {
            reply =
                    new ReplyObject(
                            HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                            e.getMessage());
        }
        return reply;
    }

}
